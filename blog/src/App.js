import React from 'react';
import logo from './logo.svg';
import './App.css';
import {HashRouter, Route, Switch} from 'react-router-dom';

import Login from "./pages/Login"
import Main from "./pages/Main"

window.BASEURI='https://gitee.com'

function App() {
  return (
    <HashRouter>
        <Switch>
            <Route exact path='/' component={Login}/>
            <Route exact path='/main' component={Main}/>
        </Switch>
    </HashRouter>
    
  );
}

export default App;
