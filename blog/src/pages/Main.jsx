import React, {Component} from "react"
import {Empty} from "antd"

export default class Main extends Component {
    render(){
        return (
            <div className='flex' style={{height:'100%',justifyContent:'center',alignItems:'center'}}>
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} imageStyle={{height:120}} description="还没有写"></Empty>
            </div>
        )
    }
}