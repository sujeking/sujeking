import React, { Component } from "react"
import { Tabs, Table, Affix, List, Drawer } from "antd"
import axios from "axios"
import ReactMarkdown from "react-markdown"
import marked from "marked"
import MD from 'markdown-it'
import "./Login.css"
const BGIMG = require('./f452955829e14ab6b1a76fc943651aae.jpg')


let md = new MD({
    html: true,        // 在源码中启用 HTML 标签
    xhtmlOut: true,        // 使用 '/' 来闭合单标签 （比如 <br />）。
    breaks: true,        // 转换段落里的 '\n' 到 <br>。
    linkify: true,        // 将类似 URL 的文本自动转换为链接。
})

function ArtcleView(props) {
    let { name, content, visible, closeAction } = props
    if (content == undefined) {
        return null;
    }
    let decodestr = decodeURIComponent(escape(window.atob(content)))
    return (
        <Drawer
            title={name}
            onClose={closeAction}
            width='50%'
            visible={visible} >

            <ReactMarkdown source={decodestr}
                escapeHtml={true}
                style={{ flex: 1 }} />

        </Drawer>
    )
}

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tree: [],
            artcles: [],
            visible: false,
            file: {},
            fileName: '',
        }
    }
    componentDidMount() {
        this.netFetchCateList()
    }

    // NET

    netFetchCateList = () => {
        let self = this
        axios.get(window.BASEURI + '/api/v5/repos/sujeking/smd/git/trees/master').then(res => {
            let status = res['status']
            if (status == 200) {
                let data = res['data']
                let tree = data['tree']
                let obj = tree[0]
                self.netFetchArtclesList(obj.path)
                this.setState({ tree });
            } else {
                alert('错误,请稍后重试')
            }
        });
    }

    netFetchArtclesList = (path) => {
        axios.get(window.BASEURI + `/api/v5/repos/sujeking/smd/contents/${path}`).then(res => {
            let status = res['status']
            if (status == 200) {
                let data = res['data']
                this.setState({ artcles: Array.isArray(data) ? data : [data] });
            } else {
                alert('错误,请稍后重试')
            }
        });
    }

    netFetchFileContent = (sha) => {
        axios.get(window.BASEURI + `/api/v5/repos/sujeking/smd/git/blobs/${sha}`).then(res => {
            let status = res['status']
            if (status == 200) {
                let data = res['data']
                this.setState({ file: data, visible: true });
            } else {
                alert('错误,请稍后重试')
            }
        });
    }

    // action
    tabChange = () => {

    }
    cateItemClick = (item) => {
        this.netFetchArtclesList(item.path)
    }

    artcleItemClick = (item) => {
        this.setState({ fileName: item.name });
        this.netFetchFileContent(item.sha)
    }

    render() {
        let { tree, artcles, visible, file, fileName } = this.state
        let body_height = document.body.clientHeight;
        let top_height = body_height * 0.1
        let bottom_height = body_height * 0.1
        return (
            <div style={{ height: body_height, backgroundImage: `url(${BGIMG})` }} className="mainPage">
                <div className="flex" style={{ flexDirection: 'column', height: body_height, padding: 20 }}>
                    {/* top menu */}
                    <div className="flex" style={{ height: top_height, alignItems: 'center', backgroundColor: '#ffffff' }}>
                        <div className="flex" style={{ flex: 1, alignItems: 'center' }}>
                            <div style={{ width: 'auto', marginRight: 20, fontWeight: 'bold', fontSize: 38 }}>sujeking</div>
                            <div style={{ fontSize: 20 }}>小笔记</div>
                        </div>
                        <div style={{ flex: 1, backgroundColor: '#000000' }}>
                        </div>
                    </div>
                    <div style={{ height: 1, backgroundColor: "#e5e5e5" }}></div>
                    {/* body */}
                    <div className='flex' style={{ flex: 1, backgroundColor: '#ffffff' }}>
                        {/* left */}
                        <div style={{ flex: 1, backgroundColor: '#ffffff' }}>
                            <List style={{ flex: 1 }} dataSource={tree} renderItem={item => (
                                <List.Item onClick={this.cateItemClick.bind(this, item)}>
                                    <div className="flex" style={{
                                        justifyContent: 'center',
                                        fontSize: 18,
                                        cursor: 'pointer',
                                        alignItems: 'center'
                                    }}>
                                        {item.path}
                                    </div>
                                </List.Item>
                            )}>

                            </List>
                        </div>
                        <div style={{ width: 1, backgroundColor: "#e5e5e5" }}></div>
                        {/* right */}
                        <div style={{ flex: 4, backgroundColor: '#fafafa' }}>
                            <List style={{ flex: 1 }} dataSource={artcles} renderItem={item => (
                                <List.Item
                                    style={{ backgroundColor: '#ffffff', }}
                                    onClick={this.artcleItemClick.bind(this, item)}>
                                    <div className="flex" style={{
                                        fontSize: 18,
                                        paddingLeft: 12,
                                        paddingRight: 12,
                                        cursor: 'pointer',
                                        alignItems: 'center'
                                    }}>
                                        {item.name}
                                    </div>
                                </List.Item>
                            )}>

                            </List>
                        </div>
                    </div>


                    <ArtcleView
                        visible={visible}
                        name={fileName}
                        content={file.content}
                        closeAction={() => {
                            this.setState({ visible: false })
                        }} />




                    {/* footer */}
                    <div style={{ height: 1, backgroundColor: "#e5e5e5" }}></div>
                    <div className="flex" style={{ height: bottom_height, backgroundColor: '#ffffff' }}>
                    </div>
                </div>
            </div>
        )
    }
}